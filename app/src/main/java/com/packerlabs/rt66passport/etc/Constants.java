package com.packerlabs.rt66passport.etc;

/**
 * Created by rzkdws on 31/07/2017.
 */

public class Constants {
    public final static String label_trip_location_name = "trip_location_name";
    public final static String label_trip_location_description = "trip_location_description";
    public final static String label_trip_location_image = "trip_location_image";
}
