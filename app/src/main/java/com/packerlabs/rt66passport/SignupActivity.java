package com.packerlabs.rt66passport;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignupActivity extends AppCompatActivity {

    @BindView(R.id.signup_email) EditText signup_email;
    @BindView(R.id.signup_password) EditText signup_password;
    @BindView(R.id.signup_birth_date) EditText signup_birth_date;
    @BindView(R.id.signup_gender) Spinner signup_gender;
    @BindView(R.id.input_layout_password) TextInputLayout passwordInputLayout;
    @BindView(R.id.input_layout_email) TextInputLayout emailInputLayout;
    @BindView(R.id.input_layout_birth) TextInputLayout birthInputLayout;
    @BindView(R.id.input_layout_gender) TextInputLayout genderInputLayout;



    private FirebaseAuth mAuth;

    final String TAG = "SignupActivity";

    String validation_error_message = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().setElevation(0);
        setContentView(R.layout.activity_create_account);
        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();

        setStatusBarColor(this);
        getSupportActionBar().setTitle(R.string.onboarding_create_account_message);
        getSupportActionBar().hide();

        signup_email.addTextChangedListener(new MyTextWatcher(emailInputLayout));
        signup_birth_date.addTextChangedListener(new MyTextWatcher(birthInputLayout));
        signup_password.addTextChangedListener(new MyTextWatcher(passwordInputLayout));
    }

    @OnClick(R.id.button_signup)
    public void selectSignupButton() {

        String email = signup_email.getText().toString().trim();
        String password = signup_password.getText().toString().trim();
        String birth_date = signup_birth_date.getText().toString();
        String gender = signup_gender.getSelectedItem().toString();

        showLoading(true);

        if (requiredDataIsValid(email, password, birth_date, gender)) {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            handleTaskResult(task);

                            showLoading(false);
                        }
                    });
        } else {
            showToast(validation_error_message, Toast.LENGTH_LONG);
        }
    }

    public void handleTaskResult(@NonNull Task<AuthResult> task){
        if (task.isSuccessful()) {
            // Sign in success, update UI with the signed-in user's information
            Log.d(TAG, "signInWithCredential:success");
            FirebaseUser user = mAuth.getCurrentUser();
            updateUI(user);
        } else {
            // If sign in fails, display a message to the user.
            Exception signup_exception = task.getException();

            showToast("Sign up failed : " + signup_exception.getMessage(), Toast.LENGTH_LONG);
        }
    }

    public void showToast(String message, int duration) {
        Toast.makeText(SignupActivity.this, message, duration).show();
    }

    public boolean requiredDataIsValid(String email, String password, String birth_date, String gender) {

        boolean result = true;

        validation_error_message = "";

        if (email.isEmpty()) {
            result = false;
            validation_error_message += "Email is required!";
            emailInputLayout.setError(validation_error_message);
        }

        if (password.isEmpty()) {
            result = false;
            validation_error_message += "\nPassword is required";
            passwordInputLayout.setError(validation_error_message);
        }

        if (birth_date.isEmpty()) {
            result = false;
            validation_error_message += "\nBirth date is required";
            birthInputLayout.setError(validation_error_message);

        }

        if (gender.isEmpty()) {
            result = false;
            validation_error_message += "\nGender is required";
        }

        return result;
    }

    public void showLoading(boolean visible) {

    }

    public void updateUI(FirebaseUser user) {
        if(user != null)
        {
            Intent mainActivity = new Intent(this, MainActivity.class);
            startActivity(mainActivity);
        }

    }

    void setStatusBarColor(Activity activity){
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(activity,R.color.colorPrimaryDark));
        }
    }
    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

            }

        }
    }
}
