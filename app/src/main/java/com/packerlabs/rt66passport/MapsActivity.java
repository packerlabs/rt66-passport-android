package com.packerlabs.rt66passport;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.packerlabs.rt66passport.model.PlaceLocation;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapsActivity.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapsActivity#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapsActivity extends Fragment implements OnMapReadyCallback, View.OnClickListener, GoogleMap.OnMarkerClickListener {

    private OnFragmentInteractionListener mListener;

    MapView map_location;

    GoogleMap mMap;

    ImageView location_logo;
    ImageView like_button;
    ImageView call_button;
    ImageView get_route_button;

    List<Marker> markers;

    TextView location_title;
    TextView location_description;

    CardView card_view;

    Animation card_animation;

    PlaceLocation selected_location;

    boolean focus_on_location;

    public MapsActivity() {
        // Required empty public constructor
    }


    public static MapsActivity newInstance() {
        MapsActivity fragment = new MapsActivity();
        Bundle args = new Bundle();
//        if(location != null){
//            args.putDouble("latitude", location.getLatitude());
//            args.putDouble("longitude", location.getLongitude());
//            args.putString("title", location.getTitle());
//            args.putString("description", location.getDescription());
//            args.putBoolean("focus_on_location", true);
//        }

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        focus_on_location = getArguments().getBoolean("focus_on_location", false);

        if(focus_on_location){
            selected_location = new PlaceLocation();
            double latitude = getArguments().getDouble("latitude");
            double longitude = getArguments().getDouble("longitude");
            String title = getArguments().getString("title");
            String description = getArguments().getString("description");
            selected_location.setDescription(description);
            selected_location.setTitle(title);
            selected_location.setLatitude(latitude);
            selected_location.setLongitude(longitude);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        map_location = ButterKnife.findById(view, R.id.map_location);
        map_location.onCreate(savedInstanceState);

        location_logo = ButterKnife.findById(view, R.id.location_logo);
        like_button = ButterKnife.findById(view, R.id.like_button);
        call_button = ButterKnife.findById(view, R.id.call_button);
        get_route_button = ButterKnife.findById(view, R.id.get_route_button);

        location_title = ButterKnife.findById(view, R.id.location_title);
        location_description = ButterKnife.findById(view, R.id.location_description);

        card_view = ButterKnife.findById(view, R.id.card_view);

        markers = new ArrayList<>();

        like_button.setOnClickListener(this);
        call_button.setOnClickListener(this);
        get_route_button.setOnClickListener(this);

        card_animation = AnimationUtils.loadAnimation(view.getContext(), R.anim.fab_scale_up);

        map_location.getMapAsync(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maps, container, false);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        if(view == like_button){
            selectLikeButton();
        }
        else if(view == get_route_button){
            selectGetRoute();
        }
        else if(view == call_button){
            selectCallButton();
        }
    }

    public void selectCallButton(){

        showToast("Selected Call button", Toast.LENGTH_SHORT);
    }

    public void selectLikeButton(){

        showToast("Selected like button", Toast.LENGTH_SHORT);
    }

    public void selectGetRoute(){
        showToast("Select GET Route", Toast.LENGTH_SHORT);
    }

    public void showToast(String message, int duration){
        Toast.makeText(getActivity(), message, duration).show();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(getActivity(), R.raw.retro_eco);
        mMap.setMapStyle(style);
        mMap.setOnMarkerClickListener(this);

        LatLng location = new LatLng(35.195885, -111.647552);
        Marker some_place = mMap.addMarker(new MarkerOptions().position(location));
        PlaceLocation sample_location = new PlaceLocation();
        sample_location.setLatitude(location.latitude);
        sample_location.setLongitude(location.longitude);
        sample_location.setTitle(getString(R.string.sample_location_title));
        sample_location.setDescription(getString(R.string.sample_location_description));
        some_place.setTag(sample_location);
        markers.add(some_place);

        if(focus_on_location)
        {
            setCenterMap(selected_location.getLocation());
            animatePlaceCardView(selected_location.getTitle(), selected_location.getDescription());
        }
        else{
            setCenterMap(location);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        map_location.onResume();
    }

    @Override
    public void onStart() {
        super.onStart();
        map_location.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
        map_location.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
        map_location.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        map_location.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        map_location.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        map_location.onSaveInstanceState(outState);
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return showPlaceCardView((PlaceLocation) marker.getTag());
    }

    public boolean showPlaceCardView(PlaceLocation marker){
        if(marker != null)
        {
            animatePlaceCardView(marker.getTitle(), marker.getDescription());
        }

        return true;
    }

    public void animatePlaceCardView(String title, String description){

        location_title.setText(title);
        location_description.setText(description);
        card_view.setVisibility(View.VISIBLE);
        card_view.startAnimation(card_animation);
    }

    public void setFocusToSelectedLocation(PlaceLocation marker){
        animatePlaceCardView(marker.getTitle(), marker.getDescription());

        setCenterMap(marker.getLocation());
    }

    public void setCenterMap(LatLng location){
        CameraUpdate center = CameraUpdateFactory.newLatLng(location);
        final int ZOOM_SETTING = 8;
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(ZOOM_SETTING);
        mMap.moveCamera(center);
        mMap.animateCamera(zoom);
    }
}
