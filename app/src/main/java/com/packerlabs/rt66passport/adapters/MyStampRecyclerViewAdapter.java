package com.packerlabs.rt66passport.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.packerlabs.rt66passport.R;
import com.packerlabs.rt66passport.PassportFragment.OnListFragmentInteractionListener;
import com.packerlabs.rt66passport.dummy.DummyContent.DummyItem;

import java.util.List;

/**
 * {@link RecyclerView.Adapter} that can display a {@link DummyItem} and makes a call to the
 * specified {@link OnListFragmentInteractionListener}.
 * TODO: Replace the implementation with code for your data type.
 */
public class MyStampRecyclerViewAdapter extends RecyclerView.Adapter<MyStampRecyclerViewAdapter.ViewHolder> {

    private final List<DummyItem> mValues;
    private final OnListFragmentInteractionListener mListener;

    ViewGroup.LayoutParams stamp_margin;

    public MyStampRecyclerViewAdapter(List<DummyItem> items, OnListFragmentInteractionListener listener, ViewGroup.LayoutParams margin) {
        mValues = items;
        mListener = listener;
        stamp_margin = margin;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_stamp, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mIdView.setText("Stamp: "+mValues.get(position).id);
        holder.mContentView.setText(mValues.get(position).content);
        holder.setContainerMargin(stamp_margin);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mIdView;
        public final TextView mContentView;
        public DummyItem mItem;

        public LinearLayout stamp_container;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mIdView = (TextView) view.findViewById(R.id.id);
            mContentView = (TextView) view.findViewById(R.id.content);
            stamp_container = (LinearLayout) view.findViewById(R.id.stamp_container);
        }

        public void setContainerMargin(ViewGroup.LayoutParams margin_params){
            stamp_container.setLayoutParams(margin_params);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mContentView.getText() + "'";
        }
    }
}
