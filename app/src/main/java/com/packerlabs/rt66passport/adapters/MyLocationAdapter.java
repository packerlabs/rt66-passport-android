package com.packerlabs.rt66passport.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.packerlabs.rt66passport.MyLocationView;
import com.packerlabs.rt66passport.R;
import com.packerlabs.rt66passport.action.OnSelectMyTripPlace;
import com.packerlabs.rt66passport.model.PlaceLocation;

import java.util.List;

/**
 * Created by rzkdws on 25/07/2017.
 */

public class MyLocationAdapter extends RecyclerView.Adapter<MyLocationView>{

    List<PlaceLocation> locations;

    LinearLayout.LayoutParams left_param;

    LinearLayout.LayoutParams right_param;

    final int NUMBER_COLUMN = 2;

    OnSelectMyTripPlace action;

    public MyLocationAdapter(List<PlaceLocation> locations, LinearLayout.LayoutParams left_param, LinearLayout.LayoutParams right_param, OnSelectMyTripPlace action) {
        this.locations = locations;
        this.left_param = left_param;
        this.right_param = right_param;
        this.action = action;
    }

    @Override
    public MyLocationView onCreateViewHolder(ViewGroup parent, int viewType) {
        View layout = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_location_item, parent, false);
        return new MyLocationView(layout, this.action);
    }

    @Override
    public void onBindViewHolder(MyLocationView holder, int position) {
        PlaceLocation location = locations.get(position);
        holder.setLocationDetail(location, getSpacingParams(position));
    }

    @Override
    public int getItemCount() {
        return locations.size();
    }

    public void setLocations(List<PlaceLocation> locations) {
        this.locations = locations;
    }

    public LinearLayout.LayoutParams getSpacingParams(int position){
        return position % NUMBER_COLUMN == 0? left_param : right_param;
    }
}
