package com.packerlabs.rt66passport.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.packerlabs.rt66passport.HomeFragment;
import com.packerlabs.rt66passport.MapsActivity;
import com.packerlabs.rt66passport.PassportFragment;
import com.packerlabs.rt66passport.ProfileFragment;
import com.packerlabs.rt66passport.model.PlaceLocation;

/**
 * Created by jeremypacker on 6/22/17.
 */

public class MainViewPagerAdapter extends FragmentStatePagerAdapter {
    int NUM_OF_TABS = 4;
    public MainViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if(position == 0) {
            return HomeFragment.newInstance();
        }else if(position == 1){
            return MapsActivity.newInstance();
        }else if(position == 2){
            return PassportFragment.newInstance(3);
        }else if(position == 3){
            return ProfileFragment.newInstance();
        }

        return new Fragment();
    }

        @Override
    public int getCount() {
        return NUM_OF_TABS;
    }
}
