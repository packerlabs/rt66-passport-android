package com.packerlabs.rt66passport.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by rzkdws on 25/07/2017.
 */

public class PlaceLocation {
    String title;

    String image_url;

    String description;

    double latitude;

    double longitude;

    LatLng location;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public LatLng getLocation() {
        return new LatLng(getLatitude(), getLongitude());
    }
}
