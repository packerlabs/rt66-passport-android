package com.packerlabs.rt66passport;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rzkdws on 16/07/2017.
 */

public class OnBoardingActivity extends AppCompatActivity{

    private FirebaseAuth mAuth;

    final String TAG = "Onboarding";

    @BindView(R.id.login_user_image) ImageView login_user_image;

    @BindView(R.id.login_button) Button login_button;

    @BindView(R.id.create_account_button) Button create_account_button;

    @BindView(R.id.logout_button) Button logout_button;

    @BindView(R.id.continue_button) Button continue_button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().setElevation(0);
        setContentView(R.layout.activity_onboarding);

        ButterKnife.bind(this);

        mAuth = FirebaseAuth.getInstance();
        getSupportActionBar().setTitle(R.string.app_name);

    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser curent_user = mAuth.getCurrentUser();
        showUserProfile(curent_user);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @OnClick(R.id.login_button)
    public void selectLogin(){
        Intent loginScreen = new Intent(this, LoginActivity.class);
        startActivity(loginScreen);
    }

    @OnClick(R.id.create_account_button)
    public void selectCreateAccount() {
        Intent signupScreen = new Intent(this, SignupActivity.class);
        startActivity(signupScreen);
    }

    @OnClick(R.id.logout_button)
    public void selectLogout(){
        mAuth.signOut();

        showUserProfile(null);
    }

    @OnClick(R.id.continue_button)
    public void selectContinueAsUser(){
        Intent addLocationScreen = new Intent(this, MainActivity.class);
        startActivity(addLocationScreen);
    }

    public void showToast(String message, int duration){
        Toast.makeText(this, message, duration).show();
    }

    public void processSignup(String email, String password){

//        String email = "van53belaz@gmail.com";
//        String password = "walking2473";

        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            showToast("Authentication failed.", Toast.LENGTH_SHORT);
                        }
                    }
                });
    }

    public void processSignin(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail", task.getException());
                            showToast("Authentication failed.", Toast.LENGTH_SHORT);
                        }

                    }
                });
    }

    public void showUserProfile(FirebaseUser user){
//        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {

            continue_button.setVisibility(View.GONE);
            logout_button.setVisibility(View.GONE);
            login_button.setVisibility(View.VISIBLE);
            create_account_button.setVisibility(View.VISIBLE);

        }
        else{

            login_button.setVisibility(View.GONE);
            create_account_button.setVisibility(View.GONE);
            logout_button.setVisibility(View.VISIBLE);
            continue_button.setVisibility(View.VISIBLE);
            String name = user.getDisplayName();
            String email = user.getEmail();
            Uri photoUrl = user.getPhotoUrl();
            continue_button.setText(getString(R.string.label_continue) + email);
        }
    }
}
