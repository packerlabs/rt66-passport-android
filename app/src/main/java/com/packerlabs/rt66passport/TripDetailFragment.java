package com.packerlabs.rt66passport;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.packerlabs.rt66passport.etc.Constants;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by rzkdws on 31/07/2017.
 */

public class TripDetailFragment extends Fragment{

    @BindView(R.id.trip_detail_image) ImageView trip_detail_image;
    @BindView(R.id.trip_detail_name) TextView trip_detail_name;
    @BindView(R.id.trip_detail_description) TextView trip_detail_description;

    String location_title;
    String location_description;
    String image_url;

    public TripDetailFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        location_title = getArguments().getString(Constants.label_trip_location_name);
        location_description = getArguments().getString(Constants.label_trip_location_description);
        image_url = getArguments().getString(Constants.label_trip_location_image);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.trip_detail, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        trip_detail_name.setText(location_title);
        trip_detail_description.setText(location_description);
        Picasso.with(view.getContext())
                .load(image_url)
                .into(trip_detail_image);
    }
}
