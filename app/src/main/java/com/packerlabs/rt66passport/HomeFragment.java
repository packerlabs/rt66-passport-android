package com.packerlabs.rt66passport;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.packerlabs.rt66passport.action.OnSelectMyTripPlace;
import com.packerlabs.rt66passport.adapters.MyLocationAdapter;
import com.packerlabs.rt66passport.model.PlaceLocation;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import in.myinnos.imagesliderwithswipeslibrary.Animations.DescriptionAnimation;
import in.myinnos.imagesliderwithswipeslibrary.SliderLayout;
import in.myinnos.imagesliderwithswipeslibrary.SliderTypes.BaseSliderView;
import in.myinnos.imagesliderwithswipeslibrary.SliderTypes.TextSliderView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements OnSelectMyTripPlace, BaseSliderView.OnSliderClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    RecyclerView my_location_list;

    MyLocationAdapter my_location_adapter;

    List<PlaceLocation> locations;

    final int LOCATION_VIEW_COLUMNS = 2;

    private OnFragmentInteractionListener mListener;

    SliderLayout mDemoSlider;

    public HomeFragment() {
        // Required empty public constructor
    }


    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        my_location_list = ButterKnife.findById(view, R.id.my_location_list);

        mDemoSlider = ButterKnife.findById(view, R.id.slider);

        locations = new ArrayList<>();

        addLocationSample();

        int spacing = getContext().getResources().getDimensionPixelOffset(R.dimen.card_margin);

        int no_spacing = 0;

        LinearLayout.LayoutParams left_side_margin = createMarginLayoutParams(no_spacing, spacing, spacing, spacing);
        LinearLayout.LayoutParams right_side_margin = createMarginLayoutParams(spacing, spacing, no_spacing, spacing);

        my_location_adapter = new MyLocationAdapter(locations, left_side_margin, right_side_margin, this);

        my_location_list.setAdapter(my_location_adapter);

        my_location_list.setLayoutManager(new GridLayoutManager(getActivity(), LOCATION_VIEW_COLUMNS));

        my_location_list.setFocusable(false);

        setupSlider();
    }

    public void setupSlider(){

        List<String> image_url = new ArrayList<>();
        image_url.add("http://grandcanyon.com/wp-content/uploads/2014/03/horseshoe-bend-page-arizona.jpg");
        image_url.add("http://res.cloudinary.com/simpleview/image/upload/v1472257613/clients/scottsdale/Stock_Grand_Canyon_084fa36e-0a2d-4560-9dbd-bbd0d93d6eb8.jpg");
        image_url.add("https://content-oars.netdna-ssl.com/wp-content/uploads/2015/12/gc-dory-lees-phantom-hero.jpg");

        for (String name : image_url) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .descriptionSize(20)
                    .image(name)
                    .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle().putString("extra", name);

            mDemoSlider.addSlider(textSliderView);
        }

        mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Stack);
        mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Top);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());
        mDemoSlider.setDuration(4000);

        mDemoSlider.setPresetTransformer("Stack");
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
//            mListener.onSelectMyTripLocation(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void showSelectedPlace(PlaceLocation detail_location) {
        mListener.onSelectMyTripLocation(detail_location);
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onSelectMyTripLocation(PlaceLocation detail_location);
    }

    public LinearLayout.LayoutParams createMarginLayoutParams(int left, int top, int right, int bottom){
        ViewGroup.MarginLayoutParams left_margin = new ViewGroup.MarginLayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

        left_margin.setMargins(left, top, right, bottom);

        return new LinearLayout.LayoutParams(new ViewGroup.MarginLayoutParams(left_margin));
    }

    public void addLocationSample(){

        for (int i = 0; i < 10; i++){
            PlaceLocation some_location = new PlaceLocation();
            some_location.setTitle("Some Location " + i);
            some_location.setImage_url("https://content.niagaracruises.com/sites/default/files/styles/hero_large/public/images/hero/carousel/HNC-NiagaraFalls-BannerSlider_1.jpg");
            some_location.setDescription("Niagara Falls (/naɪˈæɡrə/) is the collective name for three waterfalls that straddle the international border between Canada and the United States; more specifically, between the province of Ontario and the state of New York. They form the southern end of the Niagara Gorge.\n" +
                    "\n" +
                    "From largest to smallest, the three waterfalls are the Horseshoe Falls, the American Falls and the Bridal Veil Falls. The Horseshoe Falls lies on the border of the United States and Canada[1] with the American Falls entirely on the American side, separated by Goat Island. The smaller Bridal Veil Falls are also on the American side, separated from the other waterfalls by Luna Island. The international boundary line was originally drawn through Horseshoe Falls in 1819, but the boundary has long been in dispute due to natural erosion and construction.");
            some_location.setLatitude(43.085950);
            some_location.setLongitude(-79.086756);
            locations.add(some_location);
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Toast.makeText(getActivity(), slider.getBundle().get("extra") + "", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStop() {
        mDemoSlider.startAutoCycle();
        super.onStop();
    }
}
