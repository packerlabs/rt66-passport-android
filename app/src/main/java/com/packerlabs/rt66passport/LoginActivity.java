package com.packerlabs.rt66passport;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rzkdws on 20/07/2017.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    @BindView(R.id.login_email)
    EditText login_email;

    @BindView(R.id.login_password) EditText login_password;
    @BindView(R.id.input_layout_email) TextInputLayout emailLayout;
    @BindView(R.id.input_layout_password) TextInputLayout passwordLayout;
    EditText reset_email;
    Button send_reset_request;
    Button cancel_reset_request;

    private FirebaseAuth mAuth;

    final String TAG = "LoginActivity";

    AlertDialog reset_password_popup;

//    SharedPreferenceUtility preference_utility;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        getSupportActionBar().setElevation(0);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

//        preference_utility = new SharedPreferenceUtility(this);

        mAuth = FirebaseAuth.getInstance();

        setStatusBarColor(this);

        setupResetPasswordPopup();
        getSupportActionBar().setTitle(R.string.onboarding_welcome_message);
        getSupportActionBar().hide();
        login_email.addTextChangedListener(new MyTextWatcher(emailLayout));
        login_password.addTextChangedListener(new MyTextWatcher(passwordLayout));

    }

    public void setupResetPasswordPopup(){
        AlertDialog.Builder pop_up_view = new AlertDialog.Builder(this);
        View reset_password_layout = LayoutInflater.from(this).inflate(R.layout.reset_password, null);
        pop_up_view.setView(reset_password_layout);
        reset_email = ButterKnife.findById(reset_password_layout, R.id.reset_email);
        send_reset_request = ButterKnife.findById(reset_password_layout, R.id.send_reset_request);
        cancel_reset_request = ButterKnife.findById(reset_password_layout, R.id.cancel_reset_request);
        send_reset_request.setOnClickListener(this);
        cancel_reset_request.setOnClickListener(this);
        reset_password_popup = pop_up_view.create();
    }

    @OnClick(R.id.button_login)
    public void selectLoginButton(){


        String email = login_email.getText().toString();
        String password = login_password.getText().toString();

        if(email.isEmpty() || password.isEmpty())
            return;

        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        handleSigninTaskResult(task);
                    }
                });


    }

    public void selectSendResetRequest(){
        final String emailAddress = reset_email.getText().toString();

        showLoading(true);

        mAuth.sendPasswordResetEmail(emailAddress).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    showToastMessage("reset password email sent to " + emailAddress, Toast.LENGTH_SHORT);
                    Log.d(TAG, "Email sent.");
                }
                else {
                    Exception reset_exception = task.getException();
                    showToastMessage("Failed to send reset password link : " + reset_exception.getMessage(), Toast.LENGTH_LONG);
                }
                showLoading(false);
                reset_password_popup.dismiss();
            }
        });
    }

    public void selectCancelResetRequest(){
        reset_password_popup.dismiss();
    }

    @OnClick(R.id.button_forgot_password)
    public void selectForgetPasswordButton(){
        reset_password_popup.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void updateUI(FirebaseUser user){
        if(user != null){

            Intent main_menu = new Intent(this, MainActivity.class);
            startActivity(main_menu);
        }
    }

    public void showLoading(boolean visible){

    }

    public void showToastMessage(String message, int duration){
        Toast.makeText(this, message, duration).show();
    }

    public void handleSigninTaskResult(Task<AuthResult> task){
        if (task.isSuccessful()) {
            // Sign in success, update UI with the signed-in user's information
            Log.d(TAG, "signInWithCredential:success");
            FirebaseUser user = mAuth.getCurrentUser();
            updateUI(user);
        } else {
            // If sign in fails, display a message to the user.
            Log.w(TAG, "signInWithCredential:failure", task.getException());
            Exception signin_exception = task.getException();
            showToastMessage("Log in failed : " + signin_exception.getMessage(), Toast.LENGTH_SHORT);
            updateUI(null);
        }
    }

    @Override
    public void onClick(View view) {
        if(view == send_reset_request)
        {
            selectSendResetRequest();
        }
        else if(view == cancel_reset_request)
        {
            selectCancelResetRequest();
        }
    }

    void setStatusBarColor(Activity activity){
        Window window = activity.getWindow();

// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

// finally change the color
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.setStatusBarColor(ContextCompat.getColor(activity,R.color.colorPrimaryDark));
        }

    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {

            }

        }
    }
}