package com.packerlabs.rt66passport;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.packerlabs.rt66passport.action.OnSelectMyTripPlace;
import com.packerlabs.rt66passport.model.PlaceLocation;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by rzkdws on 25/07/2017.
 */

public class MyLocationView extends RecyclerView.ViewHolder implements View.OnClickListener{

    @BindView(R.id.location_image) ImageView location_image;

    @BindView(R.id.location_name) TextView location_name;

    @BindView(R.id.location_card_holder) LinearLayout location_card_holder;

    PlaceLocation model;

    OnSelectMyTripPlace action;

    public MyLocationView(View itemView, OnSelectMyTripPlace action) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.action = action;
        itemView.setOnClickListener(this);
    }

    public void setLocationDetail(PlaceLocation model, LinearLayout.LayoutParams params){
        this.model = model;

        location_name.setText(this.model.getTitle());

        Picasso.with(itemView.getContext())
                .load(model.getImage_url())
//                .placeholder(R.drawable.user_placeholder)
//                .error(R.drawable.user_placeholder_error)
                .into(location_image);

        location_card_holder.setLayoutParams(params);
    }

    public void viewLocationDetail(){
        action.showSelectedPlace(this.model);
    }

    @Override
    public void onClick(View view) {
        viewLocationDetail();
    }
}
