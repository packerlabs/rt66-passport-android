package com.packerlabs.rt66passport;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.packerlabs.rt66passport.adapters.MainViewPagerAdapter;
import com.packerlabs.rt66passport.dummy.DummyContent;
import com.packerlabs.rt66passport.etc.Constants;
import com.packerlabs.rt66passport.model.PlaceLocation;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements HomeFragment.OnFragmentInteractionListener, LocationReachedPrompt.OnFragmentInteractionListener, PassportFragment.OnListFragmentInteractionListener, ProfileFragment.OnFragmentInteractionListener, MapsActivity.OnFragmentInteractionListener {

    @BindView(R.id.navigation) BottomNavigationView mBottomNavigation;
    @BindView(R.id.contentViewPager) ViewPager mViewPager;

    MainViewPagerAdapter mPageAdapter;
    LocationReachedPrompt locationReachedPrompt;
    TripDetailFragment fragmentTripDetail;

    final int INDEX_HOME = 0;
    final int INDEX_MAP = 1;
    final int INDEX_PASSPORT = 2;
    final int INDEX_PROFILE = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setElevation(0);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setupMainViewPager();
        mBottomNavigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
    }

    /* UI Structure */
    void setupMainViewPager(){
        mPageAdapter = new MainViewPagerAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPageAdapter);
    }

    void locationReachedUI(){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        locationReachedPrompt = new LocationReachedPrompt();
        ft.replace(R.id.content
                , locationReachedPrompt, "prompt");
        ft.commit();

        getSupportActionBar().hide();
    }

    void hideLocationReached(){
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(locationReachedPrompt);
        ft.commit();
        getSupportActionBar().show();



    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                showTripDetailFragment(false, null);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /* Navigation Code */
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    updateViewPageContent(R.string.title_home, INDEX_HOME);
                    return true;
                case R.id.navigation_map:
                    updateViewPageContent(R.string.title_map, INDEX_MAP);
                    return true;
                case R.id.navigation_passport:
                    updateViewPageContent(R.string.title_passport, INDEX_PASSPORT);
                  //  hideLocationReached();
                    return true;
                case R.id.navigation_profile:
                    updateViewPageContent(R.string.title_profile, INDEX_PROFILE);
                  //  locationReachedUI();
                    return true;

            }
            return false;
        }

    };


    public void updateViewPageContent(int title_string_id, int fragment_index){
        getSupportActionBar().setTitle(title_string_id);
        mViewPager.setCurrentItem(fragment_index, true);
    }

    public void showTripDetailFragment(boolean visible, PlaceLocation detail_location){
        FragmentManager fm = getSupportFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();

        if(visible)
        {
            fragmentTripDetail = new TripDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putString(Constants.label_trip_location_description, detail_location.getDescription());
            bundle.putString(Constants.label_trip_location_name, detail_location.getTitle());
            bundle.putString(Constants.label_trip_location_image, detail_location.getImage_url());
            fragmentTripDetail.setArguments(bundle);
            ft.replace(R.id.container, fragmentTripDetail, "trip_detail");

        }
        else{
            ft.remove(fragmentTripDetail);
        }

        ft.commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(visible);
        mBottomNavigation.setVisibility(visible? View.GONE : View.VISIBLE);
    }

    /* Implement Overrides */


    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {

    }

    @Override
    public void onSelectMyTripLocation(PlaceLocation detail_location) {
        showTripDetailFragment(true, detail_location);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }
}
