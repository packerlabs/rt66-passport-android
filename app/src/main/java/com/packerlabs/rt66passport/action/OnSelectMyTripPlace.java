package com.packerlabs.rt66passport.action;

import com.packerlabs.rt66passport.model.PlaceLocation;

/**
 * Created by rzkdws on 27/07/2017.
 */

public interface OnSelectMyTripPlace {
    public void showSelectedPlace(PlaceLocation location_detail);
}
